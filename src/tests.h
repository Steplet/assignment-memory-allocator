//
// Created by Artem_Step on 23.01.2023.
//

#include "mem_internals.h"
#include "mem.h"
#include "util.h"
#include <stdio.h>
#include <unistd.h>

#ifndef C_PROJECT_CLION_TESTS_H
#define C_PROJECT_CLION_TESTS_H

bool test_1();
bool test_2();
bool test_3();
bool test_4();
bool test_5();

#endif //C_PROJECT_CLION_TESTS_H
