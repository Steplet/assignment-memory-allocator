//
// Created by Artem_Step on 23.01.2023.
//
#include "tests.h"


int main(){
    heap_init(REGION_MIN_SIZE);

    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
    return 0;
}
