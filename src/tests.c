//
// Created by Artem_Step on 23.01.2023.
//
#include "tests.h"
#include "mem.h"
#include <unistd.h>


bool test_1(){
    fprintf(stdout,"Test 1 ...");
    void * blk = _malloc(100);
    if(blk == NULL){
        fprintf(stdout,"NULL \n");
        _free(blk);
        debug_heap(stdout,HEAP_START);
        return false;
    }
    if (block_get_header(blk)->capacity.bytes == 100){
        fprintf(stdout, " passed\n");
        _free(blk);
        debug_heap(stdout,HEAP_START);
        return true;
    } else{
        fprintf(stdout, "Wrong capacity\n");
        _free(blk);
        debug_heap(stdout,HEAP_START);
        fprintf(stdout, " \n");
        return false;
    }
}

bool test_2(){
    fprintf(stdout,"Test 2 ...");
    void* blk_1 = _malloc(40);
    void * blk_2 = _malloc(200);

    _free(blk_1);


    if (!block_get_header(blk_1)->is_free){
        fprintf(stdout, "Wrong answer, check blk_1->is_free\n");
        _free(blk_2);
        debug_heap(stdout,HEAP_START);
        return false;
    }
    if (block_get_header(blk_2)->is_free){
        fprintf(stdout, "Wrong answer, check blk_2->is_free\n");
        _free(blk_2);
        debug_heap(stdout,HEAP_START);
        return false;
    }
    if(!(block_get_header(blk_2)->is_free) && block_get_header(blk_2)->capacity.bytes == 200){
        fprintf(stdout, " passed\n");
        _free(blk_2);
        debug_heap(stdout,HEAP_START);
        return true;
    }
    fprintf(stdout, "Wrong answer\n");
    fprintf(stdout, " \n");

    return false;
}

bool test_3(){

    fprintf(stdout,"Test 3 ...");
    void* blk_1 = _malloc(5);
    void * blk_2 = _malloc(200);
    void* blk_3 = _malloc(666);

    _free(blk_1);
    _free(blk_2);

    if (!block_get_header(blk_1)->is_free){
        fprintf(stdout, "Wrong answer, check blk_1->is_free\n");
        debug_heap(stdout,HEAP_START);
        return false;
    }
    if (!block_get_header(blk_2)->is_free){
        fprintf(stdout, "Wrong answer, check blk_2->is_free\n");
        debug_heap(stdout,HEAP_START);
        return false;
    }
    if (block_get_header(blk_3)->is_free){
        fprintf(stdout, "Wrong answer, check blk_3->is_free\n");
        debug_heap(stdout,HEAP_START);
        _free(blk_3);
        return false;
    }
    if (!block_get_header(blk_3)->is_free && block_get_header(blk_3)->capacity.bytes == 666){
        fprintf(stdout, " passed\n");
        debug_heap(stdout,HEAP_START);
        fprintf(stdout, " \n");
        _free(blk_3);
        return true;
    }

    fprintf(stdout, " Wrong answer\n");
    debug_heap(stdout,HEAP_START);
    fprintf(stdout, " \n");
    _free(blk_3);
    return true;
}

bool test_4(){
    fprintf(stdout,"Test 4 ...");
    void* blk_1 = _malloc(10000);
    void * blk_2 = _malloc(20000);

    if(block_get_header(blk_1)->next != block_get_header(blk_2)){
        fprintf(stdout, "Wrong answer, check blk_1->next\n");
        debug_heap(stdout,HEAP_START);
        _free(blk_1);
        _free(blk_2);
        return false;
    }
    fprintf(stdout, " passed\n");
    debug_heap(stdout,HEAP_START);
    fprintf(stdout, " \n");
    return true;
}

bool test_5(){
    fprintf(stdout,"Test 5 ...");
    void* blk_1 = _malloc(10000);
    struct block_header* block_head_1 = block_get_header(blk_1);

    while (block_head_1->next != NULL){
    block_head_1 = block_head_1->next;

    }

    mmap(block_head_1, 10000, PROT_READ | PROT_WRITE, MAP_FIXED, 0, 0);

void* blk_2 = _malloc(50000);
    if(block_head_1->capacity.bytes + block_head_1->contents ==(uint8_t *) block_get_header(blk_2)){
        fprintf(stdout, "Wrong answer\n");
        debug_heap(stdout,HEAP_START);
        _free(blk_1);
        _free(blk_2);
        return false;
    }
    fprintf(stdout, " passed\n");
    debug_heap(stdout,HEAP_START);
    fprintf(stdout, " \n");
    return true;

}

